from fuzzy_system.fuzzy_variable_output import FuzzyOutputVariable
from fuzzy_system.fuzzy_variable_input import FuzzyInputVariable
from fuzzy_system.fuzzy_system import FuzzySystem
from optimizers.utils import Vector_processing


class FuzzyVehicleInference:
    def __init__(self, MAX_DELAY_REMAIN, MAX_PACKET_SIZE,  MAX_QUEUE_SIZE_C=None, MAX_QUEUE_SIZE_R=None, MAX_QUEUE_SIZE_G=None):
        self.MAX_DELAY_REMAIN = MAX_DELAY_REMAIN
        self.MAX_PACKET_SIZE = MAX_PACKET_SIZE
        self.MAX_QUEUE_SIZE_C = 100
        self.MAX_QUEUE_SIZE_R = 500
        self.MAX_QUEUE_SIZE_G = 1000
        self.system = None
        self.initFuzzySystem()
        self.vehicleRule()

    def initFuzzySystem(self):
        ### INPUT
        delayRemain = FuzzyInputVariable('Delay Remain', 0, self.MAX_DELAY_REMAIN, 100)
        delayRemain.add_trapezoidal('S', 0, 0, 0.003, 0.005)
        delayRemain.add_trapezoidal('M', 0, 0.004, 0.006, 0.008)
        delayRemain.add_trapezoidal('H', 0.007, 0.009, self.MAX_DELAY_REMAIN, self.MAX_DELAY_REMAIN)

        packageSize = FuzzyInputVariable('Package Size', 0, self.MAX_PACKET_SIZE, 100)
        packageSize.add_trapezoidal('S', 0, 0, 1, 2)
        packageSize.add_trapezoidal('M', 1, 3, 4, 6)
        packageSize.add_trapezoidal('H', 5, 6, self.MAX_PACKET_SIZE, self.MAX_PACKET_SIZE)

        queueCar = FuzzyInputVariable("Queue_car Size", 0, self.MAX_QUEUE_SIZE_C, 100 * self.MAX_QUEUE_SIZE_C)
        queueCar.add_trapezoidal('M', 0, 0, 0.25 * self.MAX_QUEUE_SIZE_C, 0.75 * self.MAX_QUEUE_SIZE_C)
        queueCar.add_trapezoidal('H', 0.25 * self.MAX_QUEUE_SIZE_C, 0.75 * self.MAX_QUEUE_SIZE_C, self.MAX_QUEUE_SIZE_C, self.MAX_QUEUE_SIZE_C)

        queueRsu = FuzzyInputVariable("Queue_rsu Size", 0, self.MAX_QUEUE_SIZE_R, 100 * self.MAX_QUEUE_SIZE_R)
        queueRsu.add_trapezoidal('M', 0, 0, 0.25 * self.MAX_QUEUE_SIZE_R, 0.75 * self.MAX_QUEUE_SIZE_R)
        queueRsu.add_trapezoidal('H', 0.25 * self.MAX_QUEUE_SIZE_R, 0.75 * self.MAX_QUEUE_SIZE_R, self.MAX_QUEUE_SIZE_R, self.MAX_QUEUE_SIZE_R)

        queueGnb = FuzzyInputVariable("Queue_gnb Size", 0, self.MAX_QUEUE_SIZE_G, 100 * self.MAX_QUEUE_SIZE_G)
        queueGnb.add_trapezoidal('M', 0, 0, 0.25 * self.MAX_QUEUE_SIZE_G, 0.75 * self.MAX_QUEUE_SIZE_G)
        queueGnb.add_trapezoidal('H', 0.25 * self.MAX_QUEUE_SIZE_G, 0.75 * self.MAX_QUEUE_SIZE_G, self.MAX_QUEUE_SIZE_G, self.MAX_QUEUE_SIZE_G)

        queueSelf = FuzzyInputVariable("Queue_self Size", 0, self.MAX_QUEUE_SIZE_R, 100 * self.MAX_QUEUE_SIZE_R)
        queueSelf.add_trapezoidal('M', 0, 0, 0.25 * self.MAX_QUEUE_SIZE_R, 0.75 * self.MAX_QUEUE_SIZE_R)
        queueSelf.add_trapezoidal('H', 0.25 * self.MAX_QUEUE_SIZE_R, 0.75 * self.MAX_QUEUE_SIZE_R, self.MAX_QUEUE_SIZE_R, self.MAX_QUEUE_SIZE_R)

        ### OUTPUT
        p_car = FuzzyOutputVariable('Car Probability', 0, 1, 100)
        p_car.add_trapezoidal('S', 0, 0, 0.1, 0.2)
        p_car.add_trapezoidal('MS', 0.1, 0.2, 0.3, 0.4)
        p_car.add_trapezoidal('M', 0.3, 0.4, 0.5, 0.6)
        p_car.add_trapezoidal('MH', 0.5, 0.6, 0.7, 0.8)
        p_car.add_trapezoidal('H', 0.7, 0.8, 1, 1)

        p_RSU = FuzzyOutputVariable('Rsu Probability', 0, 1, 100)
        p_RSU.add_trapezoidal('S', 0, 0, 0.1, 0.2)
        p_RSU.add_trapezoidal('MS', 0.1, 0.2, 0.3, 0.4)
        p_RSU.add_trapezoidal('M', 0.3, 0.4, 0.5, 0.6)
        p_RSU.add_trapezoidal('MH', 0.5, 0.6, 0.7, 0.8)
        p_RSU.add_trapezoidal('H', 0.7, 0.8, 1, 1)

        p_gnb = FuzzyOutputVariable('Gnb Probability', 0, 1, 100)
        p_gnb.add_trapezoidal('S', 0, 0, 0.1, 0.2)
        p_gnb.add_trapezoidal('MS', 0.1, 0.2, 0.3, 0.4)
        p_gnb.add_trapezoidal('M', 0.3, 0.4, 0.5, 0.6)
        p_gnb.add_trapezoidal('MH', 0.5, 0.6, 0.7, 0.8)
        p_gnb.add_trapezoidal('H', 0.7, 0.8, 1, 1)

        p_self = FuzzyOutputVariable('Self Probability', 0, 1, 100)
        p_self.add_trapezoidal('S', 0, 0, 0.1, 0.2)
        p_self.add_trapezoidal('MS', 0.1, 0.2, 0.3, 0.4)
        p_self.add_trapezoidal('M', 0.3, 0.4, 0.5, 0.6)
        p_self.add_trapezoidal('MH', 0.5, 0.6, 0.7, 0.8)
        p_self.add_trapezoidal('H', 0.7, 0.8, 1, 1)

        ### INIT FUZZY SYSTEM
        self.system = FuzzySystem()
        self.system.add_input_variable(delayRemain)
        self.system.add_input_variable(packageSize)
        self.system.add_input_variable(queueCar)
        self.system.add_input_variable(queueRsu)
        self.system.add_input_variable(queueGnb)
        self.system.add_input_variable(queueSelf)
        self.system.add_output_variable(p_car)
        self.system.add_output_variable(p_RSU)
        self.system.add_output_variable(p_gnb)
        self.system.add_output_variable(p_self)

    def vehicleRule(self):
        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'S',
             'Gnb Probability': 'S',
             'Self Probability': 'H'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'MS',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'M',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MS',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MH',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'MH'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'S',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'M',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MH',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'S',
             'Gnb Probability': 'S',
             'Self Probability': 'H'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'S',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'M',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'S',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MH',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'MH'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'S',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'S',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'M',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'S',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MH',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'S',
             'Gnb Probability': 'S',
             'Self Probability': 'H'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MS',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'M',
             'Rsu Probability': 'MS',
             'Gnb Probability': 'MS',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MH',
             'Rsu Probability': 'MS',
             'Gnb Probability': 'MS',
             'Self Probability': 'MH'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'M',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'S',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MS',
             'Gnb Probability': 'S',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'M',
             'Rsu Probability': 'MS',
             'Gnb Probability': 'MS',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MH',
             'Rsu Probability': 'MS',
             'Gnb Probability': 'MS',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'M',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'S',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MS',
             'Gnb Probability': 'S',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'M',
             'Rsu Probability': 'MS',
             'Gnb Probability': 'S',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'S',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MH',
             'Rsu Probability': 'MS',
             'Gnb Probability': 'MS',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'M',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'H',
             'Queue_car Size': 'M',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'S',
             'Gnb Probability': 'S',
             'Self Probability': 'H'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'MS',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MS',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'MH'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'S',
             'Gnb Probability': 'S',
             'Self Probability': 'MH'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'S',
             'Gnb Probability': 'S',
             'Self Probability': 'H'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'S',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'S',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'M',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'MH'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'S',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'S',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'S',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'M',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'M',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'S',
             'Gnb Probability': 'S',
             'Self Probability': 'H'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MS',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MS',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MS',
             'Gnb Probability': 'MS',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MS',
             'Gnb Probability': 'MS',
             'Self Probability': 'MH'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'M',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'S',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MS',
             'Gnb Probability': 'S',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MS',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MS',
             'Gnb Probability': 'MS',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'M',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'M',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'S',
             'Gnb Probability': 'S',
             'Self Probability': 'H'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'S',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'M',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'S',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MH',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'MH'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MS',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'M'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'S',
             'Gnb Probability': 'S',
             'Self Probability': 'H'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'M',
             'Gnb Probability': 'S',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'S',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'MS',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'M',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'M'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'MS',
             'Self Probability': 'MS'})

        self.system.add_rule(
            {'Delay Remain': 'M',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'S',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MH',
             'Rsu Probability': 'M',
             'Gnb Probability': 'MS',
             'Self Probability': 'MH'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'M',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'MS',
             'Rsu Probability': 'MH',
             'Gnb Probability': 'M',
             'Self Probability': 'S'})

        self.system.add_rule(
            {'Delay Remain': 'H',
             'Package Size': 'H',
             'Queue_car Size': 'H',
             'Queue_rsu Size': 'H',
             'Queue_gnb Size': 'H',
             'Queue_self Size': 'H'},
            {'Car Probability': 'S',
             'Rsu Probability': 'H',
             'Gnb Probability': 'MH',
             'Self Probability': 'S'})

    def Inference(self, fuzzy_input=None, showResult=False, plot=False):
            delayRemain = fuzzy_input["dR"]
            packageSize = fuzzy_input["size"]
            queueCar = fuzzy_input['qC']
            queueRsu = fuzzy_input['qR']
            queueGnb = fuzzy_input['qG']
            queueSelf = fuzzy_input['qS']

            if delayRemain > self.MAX_DELAY_REMAIN:
                delayRemain = self.MAX_DELAY_REMAIN
            if packageSize > self.MAX_PACKET_SIZE:
                packageSize = self.MAX_PACKET_SIZE
            if queueCar > self.MAX_QUEUE_SIZE_C:
                queueCar = self.MAX_QUEUE_SIZE_C
            if queueRsu > self.MAX_QUEUE_SIZE_R:
                queueRsu = self.MAX_QUEUE_SIZE_R
            if queueGnb > self.MAX_QUEUE_SIZE_G:
                queueGnb = self.MAX_QUEUE_SIZE_G
            if queueSelf > self.MAX_QUEUE_SIZE_C:
                queueSelf = self.MAX_QUEUE_SIZE_C

            #print("delayRemain: " + str(delayRemain))
            #print("packageSize: " + str(packageSize))

            output = self.system.evaluate_output({
                'Delay Remain': delayRemain,
                'Package Size': packageSize,
                'Queue_car Size': queueCar,
                'Queue_rsu Size': queueRsu,
                'Queue_gnb Size': queueGnb,
                'Queue_self Size': queueSelf
            })

            outputVector = [output['Car Probability'], output['Rsu Probability'],
                            output['Gnb Probability'], output['Self Probability']]

            outputVector = Vector_processing.percentage(outputVector)

            if showResult:
                print(outputVector)
            if plot:
                self.system.plot_system()

            return outputVector

### TEST
#FuzzySystemTest = FuzzyVehicleInference(1, 10)
#FuzzySystemTest.Inference(0.1, 10, True, False)
