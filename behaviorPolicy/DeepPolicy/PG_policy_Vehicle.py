from config import Config
from optimizers.Fuzzy.fuzzy_inference_vehicle import FuzzyVehicleInference
import numpy as np


class PG_policy_car:
    car_fuzzy_system = FuzzyVehicleInference(MAX_DELAY_REMAIN=Config.maxDelay,
                                             MAX_PACKET_SIZE=Config.maxSize,
                                             MAX_QUEUE_SIZE_C=100,
                                             MAX_QUEUE_SIZE_R=500,
                                             MAX_QUEUE_SIZE_G=1000)

    def __init__(self):
        self.Fuzzy_factor = 0.3
        self.Fuzzy_decay = 0.975

    def getPolicy(self):
        def chooseAction(exclude_actions=None, queue_infor=None, message=None, car_id=None, Vnet=None):
            dR = message.getDelayRemain()
            size = message.size
            queue_car = queue_infor["car"]
            queue_rsu = queue_infor["rsu"]
            queue_gnb = queue_infor["gnb"]
            queue_self = queue_infor["self"]
            FuzzyInput = {
                "dR": dR,
                "size": size,
                "qC": queue_car,
                "qR": queue_rsu,
                "qG": queue_gnb,
                "qS": queue_self
            }
            FuzzyOutput = 0
            if self.Fuzzy_factor > 0.001:
                FuzzyOutput = PG_policy_car.car_fuzzy_system.Inference(fuzzy_input=FuzzyInput)

            NetInput = np.array(list(FuzzyInput.values())).reshape([1, Vnet.numInput])
            NetOutput = Vnet.predict(message_id=message.message_id, object_id=car_id, vector=NetInput)

            probVector = self.Fuzzy_factor * FuzzyOutput + (1 - self.Fuzzy_factor) * NetOutput
            self.Fuzzy_factor *= self.Fuzzy_decay
            # make action by Fuzzy choice
            # print(FuzzyInput)
            # print(FuzzyOutput)
            probVector[0][-1] = 1 - np.array(probVector[0][:-1]).sum()
            action = np.random.choice(a=[0, 1, 2, 3], p=np.array(probVector).reshape([4, ]))
            while action in exclude_actions:
                action = np.random.choice(a=[0, 1, 2, 3], p=np.array(probVector).reshape([4, ]))

            return action
        return chooseAction
