from config import Config
from optimizers.Fuzzy.fuzzy_inference_rsu import FuzzyRsuInference
import numpy as np


class PG_policy_rsu:
    rsu_fuzzy_system = FuzzyRsuInference(MAX_DELAY_REMAIN=Config.maxDelay,
                                         MAX_PACKET_SIZE=Config.maxSize,
                                         MAX_QUEUE_SIZE_R=500,
                                         MAX_QUEUE_SIZE_G=1000)

    """
    With decay = 0.995, starts at 0.3, it takes ~1140 times update
    to decay almost all fuzzy system
    """
    def __init__(self):
        self.Fuzzy_factor = 1
        self.Fuzzy_decay = 0.995

    def getPolicy(self):
        def chooseAction(exclude_actions=None, queue_infor=None, message=None, rsu_id=None, Rnet=None):
            dR = message.getDelayRemain()
            size = message.size
            queue_rsu = queue_infor["rsu"]
            queue_gnb = queue_infor["gnb"]
            queue_self = queue_infor["self"]

            FuzzyInput = {
                "dR": dR,
                "size": size,
                "qR": queue_rsu,
                "qG": queue_gnb,
                "qS": queue_self
            }
            FuzzyOutput = 0
            if self.Fuzzy_factor > 0.001:
                FuzzyOutput = PG_policy_rsu.rsu_fuzzy_system.Inference(fuzzy_input=FuzzyInput)

            NetInput = np.array(list(FuzzyInput.values())).reshape([1, Rnet.numInput])
            NetOutput = Rnet.predict(message_id=message.message_id, object_id=rsu_id, vector=NetInput)

            # print(NetOutput)
            # print(FuzzyOutput)
            probVector = self.Fuzzy_factor * FuzzyOutput + (1 - self.Fuzzy_factor) * NetOutput
            self.Fuzzy_factor *= self.Fuzzy_decay
            # make action by Fuzzy choice
            probVector[0][-1] = 1 - np.array(probVector[0][:-1]).sum()
            action = np.random.choice(a=[1, 2, 3], p=np.array(probVector).reshape([3, ]))
            while action in exclude_actions:
                action = np.random.choice(a=[1, 2, 3], p=np.array(probVector).reshape([3, ]))

            return action
        return chooseAction